Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp3 = System.Tuple(Of Integer, Integer, Integer)
Imports BitArray = System.Collections.BitArray
Imports Solution = System.Tuple(Of System.Collections.BitArray, Integer)

Public Class PointsOnGrid
    Const NoArea As Integer = -(100 * 100)
    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Dim H, W, subH, subW, Kmin, Kmax, exH, exW As Integer
    Dim field() As Integer
    Dim points(9)() As Integer
    Dim pIndexes(9)() As Integer

    Public Function findSolution(H As Integer, W As Integer, subH As Integer, subW As Integer, Kmin As Integer, Kmax As Integer, grid() As String) As String()
        startTime = Environment.TickCount
        limitTime = startTime + 9800
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        Me.H = H
        Me.W = W
        Me.subH = subH
        Me.subW = subW
        Me.Kmin = Kmin
        Me.Kmax = Kmax
        exH = H + subH
        exW = W + subW

        InitField(grid)

        Dim perfectScore As Integer = CalcMaxScore()
        Console.Error.WriteLine("MaxScore: {0}", perfectScore)

        Dim paints As Solution = GetLocal1stSolution()
        Console.Error.WriteLine("local-1th: {0}", paints.Item2)

        If paints.Item2 = perfectScore Then
            findSolution = ConvertSolution(paints)
            Exit Function
        End If

        Dim basePaints As BitArray = paints.Item1
        Dim baseScore As Integer = paints.Item2
        Dim countMap() As Integer = GetCountMap(basePaints)
        Dim tmp As Solution = Nothing

        For n As Integer = 0 To 6
            tmp = GetLocal3rdTo9thSolution(basePaints, countMap, n)
            Console.Error.WriteLine("local-{0}th: {1}", n + 3, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
        Next n

        For n As Integer = 1 To 6
            tmp = GetLocal10thTo15thSolution(basePaints, countMap, n)
            Console.Error.WriteLine("local-{0}th: {1}", n + 9, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
        Next n

        tmp = GetLocal16thSolution()
        Console.Error.WriteLine("local-16th: {0}", tmp.Item2)
        If tmp.Item2 > paints.Item2 Then
            paints = tmp
            If paints.Item2 = perfectScore Then
                findSolution = ConvertSolution(paints)
                Exit Function
            End If
        End If

        For n As Integer = 2 To 5
            tmp = GetLocal17thTo20thSolution(basePaints, countMap, n)
            Console.Error.WriteLine("local-{0}th: {1}", n + 15, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
        Next n

        tmp = GetLocal21stSolution(basePaints, countMap)
        Console.Error.WriteLine("local-21th: {0}", tmp.Item2)
        If tmp.Item2 > paints.Item2 Then
            paints = tmp
            If paints.Item2 = perfectScore Then
                findSolution = ConvertSolution(paints)
                Exit Function
            End If
        End If

        tmp = GetLocal23rdSolution(paints, 50)
        Console.Error.WriteLine("local-23th: {0}", tmp.Item2)
        If tmp.Item2 > paints.Item2 Then
            paints = tmp
            If paints.Item2 = perfectScore Then
                findSolution = ConvertSolution(paints)
                Exit Function
            End If
        End If
        tmp = GetLocal23rdSolution(New Solution(basePaints, baseScore), 50)
        Console.Error.WriteLine("local-23th-2: {0}", tmp.Item2)
        If tmp.Item2 > paints.Item2 Then
            paints = tmp
            If paints.Item2 = perfectScore Then
                findSolution = ConvertSolution(paints)
                Exit Function
            End If
        End If

        For cycle As Integer = 1 To 30
            tmp = GetLocal24thSolution(20)
            Console.Error.WriteLine("local-24th-{0}: {1}", cycle, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
        Next cycle

        tmp = New Solution(basePaints, baseScore)
        For n As Integer = 1 To 5
            For cycle As Integer = 1 To 10
                tmp = GetLocal25thSolution(tmp, 10)
                tmp = GetLocal23rdSolution(tmp, 10)
            Next cycle
            Console.Error.WriteLine("local-25th-{0}: {1}",  n, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
            tmp = GetLocal24thSolution(10)
            Console.Error.WriteLine("local-24th-{0}: {1}", n + 30, tmp.Item2)
            If tmp.Item2 > paints.Item2 Then
                paints = tmp
                If paints.Item2 = perfectScore Then
                    findSolution = ConvertSolution(paints)
                    Exit Function
                End If
            End If
        Next n

        tmp = GetLocal26thSolution(basePaints, baseScore, countMap)
        Console.Error.WriteLine("local-26th: {0}", tmp.Item2)
        If tmp.Item2 > paints.Item2 Then
            paints = tmp
            If paints.Item2 = perfectScore Then
                findSolution = ConvertSolution(paints)
                Exit Function
            End If
        End If


        findSolution = ConvertSolution(paints)
    End Function

    Private Function ConvertSolution(sol As Solution) As String()
        Dim ret(H - 1) As String

        For i As Integer = 0 To H - 1
            Dim s As String = ""
            For j As Integer = 0 To W - 1
                s += If(sol.Item1(i * W + j), "x", ".")
            Next j
            ret(i) = s
        Next i

        ConvertSolution = ret
    End Function

    Private Sub Shuffle(Of T)(xs() As T)
        For i As Integer = UBound(xs) To 1 Step -1
            Dim j As Integer = rand.Next(i + 1)
            Dim x As T = xs(i)
            xs(i) = xs(j)
            xs(j) = x
        Next i
    End Sub

    Private Sub InitField(grid() As String)
        Dim ps(9) As List(Of Integer)
        For i As Integer = 0 To UBound(ps)
            ps(i) = New List(Of Integer)()
        Next i
        ReDim field(H * W - 1)
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                field(i * W + j) = Asc(grid(i)(j)) - Asc("0"c)
                ps(field(i * W + j)).Add(i * W + j)
            Next j
        Next i
        For i As Integer = 0 To UBound(ps)
            points(i) = ps(i).ToArray()
            ReDim pIndexes(i)(UBound(points(i)))
            For j As Integer = 0 To UBound(pIndexes(i))
                pIndexes(i)(j) = j
            Next j
            Shuffle(pIndexes(i))
        Next i
    End Sub

    Private Function CalcMaxScore() As Integer
        Dim ch As Integer = H \ subH
        Dim cw As Integer = W \ subW
        Dim rh As Integer = H Mod subH
        Dim rw As Integer = W Mod subW
        Dim rside As Integer = ch * subH * rw
        Dim bside As Integer = cw * rh * subW
        Dim cross As Integer = rh * rw
        Dim k As Integer = Kmax
        Dim kc As Integer = Math.Min(k, cross)
        k -= kc
        Dim kr, kb As Integer
        If rside > bside Then
            kr = Math.Min(k, subH * rw - cross)
            k -= kr
            kb = Math.Min(k, rh * subW - cross)
        Else
            kb = Math.Min(k, rh * subW - cross)
            k -= kb
            kr = Math.Min(k, subH * rw - cross)
        End If
        Dim count As Integer = Kmax * ch * cw + kc + (kr + kc) * ch + (kb + kc) * cw
        Dim es() As Integer = TryCast(field.Clone(), Integer())
        Array.Sort(es)
        Dim score As Integer = 0
        For i As Integer = 1 To count
            score += es(es.Length - i)
        Next i
        CalcMaxScore = score
    End Function

    Private Function GetScore(paints As BitArray) As Integer
        Dim score As Integer = 0
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If paints(i * W + j) Then
                    score += field(i * W + j)
                End If
            Next j
        Next i
        GetScore = score
    End Function

    Private Function GetSimplePatternPaints(endK As Integer) As BitArray
        Dim paints As New BitArray(H * W)

        Dim ps As New List(Of Tp3)()

        For i As Integer = 0 To subH - 1
            For j As Integer = 0 To subW - 1
                Dim sum As Integer = 0
                For y As Integer = i To H - 1 Step subH
                    For x As Integer = j To W - 1 Step subW
                        sum += field(y * W + x)
                    Next x
                Next y
                ps.Add(New Tp3(sum, i, j))
            Next j
        Next i

        ps.Sort()

        For k As Integer = 1 To endK
            Dim sel As Tp3 = ps(ps.Count - k)
            For y As Integer = sel.Item2 To H - 1 Step subH
                For x As Integer = sel.Item3 To W - 1 Step subW
                    paints(y * W + x) = True
                Next x
            Next y
        Next k

        GetSimplePatternPaints = paints
    End Function

    Private Function GetCountMap(paints As BitArray, Optional countMap() As Integer = Nothing) As Integer()
        If countMap Is Nothing Then
            ReDim countMap(exH * exW - 1)
        End If
        For i As Integer = 0 To UBound(countMap)
            countMap(i) = 2 * NoArea
        Next i
        For y As Integer = 0 To H - 1 - (subH - 1)
            For x As Integer = 0 To W - 1 - (subW - 1)
                Dim count As Integer = 0
                For dy As Integer = 0 To subH - 1
                    For dx As Integer = 0 To subW - 1
                        If paints((y + dy) * W + (x + dx)) Then count += 1
                    Next dx
                Next dy
                countMap((y + subH - 1) * exW + (x + subW - 1)) = count
            Next x
        Next y
        GetCountMap = countMap
    End Function

    Private Function CanValidRemove(countMap() As Integer, y As Integer, x As Integer) As Boolean
        For dy As Integer = 0 To subH - 1
            Dim i As Integer = (y + dy) * exW + x
            For p As Integer = i To i + subW - 1
                Dim c As Integer = countMap(p)
                If c <= Kmin AndAlso c > NoArea Then
                    CanValidRemove = False
                    Exit Function
                End If
            Next p
        Next dy
        CanValidRemove = True
    End Function

    Private Function CanValidPaint(countMap() As Integer, y As Integer, x As Integer) As Boolean
        For dy As Integer = 0 To subH - 1
            Dim i As Integer = (y + dy) * exW + x
            For p As Integer = i To i + subW - 1
                If countMap(p) >= Kmax Then
                    CanValidPaint = False
                    Exit Function
                End If
            Next p
        Next dy
        CanValidPaint = True
    End Function

    Private Sub Remove(paints As BitArray, countMap() As Integer, y As Integer, x As Integer)
        paints(y * W + x) = False
        For dy As Integer = 0 To subH - 1
            Dim i As Integer = (y + dy) * exW + x
            For p As Integer = i To i + subW - 1
                countMap(p) -= 1
            Next p
        Next dy
    End Sub

    Private Sub Paint(paints As BitArray, countMap() As Integer, y As Integer, x As Integer)
        paints(y * W + x) = True
        For dy As Integer = 0 To subH - 1
            Dim i As Integer = (y + dy) * exW + x
            For p As Integer = i To i + subW - 1
                countMap(p) += 1
            Next p
        Next dy
    End Sub

    Private Sub RemoveGreedy(paints As BitArray, countMap() As Integer, maxRemoveNumber As Integer)
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If Not paints(y * W + x) Then Continue For
                If field(y * W + x) > maxRemoveNumber Then Continue For
                If CanValidRemove(countMap, y, x) Then
                    Remove(paints, countMap, y, x)
                End If
            Next x
        Next y
    End Sub

    Private Function IncreaseRemoveGreedy(paints As BitArray, countMap() As Integer, maxRemoveNumber As Integer) As Integer
        Dim count As Integer = 0
        For n As Integer = 0 To maxRemoveNumber
            For y As Integer = 0 To H - 1
                For x As Integer = 0 To W - 1
                    If Not paints(y * W + x) Then Continue For
                    If field(y * W + x) <> n Then Continue For
                    If CanValidRemove(countMap, y, x) Then
                        Remove(paints, countMap, y, x)
                        count -= n
                    End If
                Next x
            Next y
        Next n
        IncreaseRemoveGreedy = count
    End Function

    Private Function FillGreedy(paints As BitArray, countMap() As Integer) As Integer
        Dim count As Integer = 0
        For n As Integer = 9 To 1 Step - 1
            For y As Integer = 0 To H - 1
                For x As Integer = 0 To W - 1
                    If paints(y * W + x) Then Continue For
                    If field(y * W + x) <> n Then Continue For
                    If CanValidPaint(countMap, y, x) Then
                        Paint(paints, countMap, y, x)
                        count += n
                    End If
                Next x
            Next y
        Next n
        FillGreedy = count
    End Function

    Private Function GetLocal21stSolution(paints As BitArray, countMap() As Integer) As Solution
        Dim bestPaints As BitArray = paints
        Dim bestDiff As Integer = 0
        Dim curDiff As Integer = 0
        paints = New BitArray(paints)
        countMap = TryCast(countMap.Clone(), Integer())
        For cycle As Integer = 1 To 7
            curDiff -= IncreaseRemoveGreedy(paints, countMap, 9 - cycle)
            curDiff += FillGreedy(paints, countMap)
            If curDiff > bestDiff Then
                bestDiff = curDiff
                bestPaints = New BitArray(paints)
            End If
        Next cycle
        paints = bestPaints
        Dim score As Integer = GetScore(paints)
        GetLocal21stSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal17thTo20thSolution(paints As BitArray, countMap() As Integer, maxCycle As Integer) As Solution
        Dim bestPaints As BitArray = paints
        Dim bestDiff As Integer = 0
        Dim curDiff As Integer = 0
        paints = New BitArray(paints)
        countMap = TryCast(countMap.Clone(), Integer())
        For cycle As Integer = 1 To maxCycle
            curDiff -= IncreaseRemoveGreedy(paints, countMap, 5)
            curDiff += FillGreedy(paints, countMap)
            If curDiff > bestDiff Then
                bestDiff = curDiff
                bestPaints = New BitArray(paints)
            End If
        Next cycle
        paints = bestPaints
        Dim score As Integer = GetScore(paints)
        GetLocal17thTo20thSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal16thSolution() As Solution
        Dim paints As BitArray = GetSimplePatternPaints(Kmin)
        Dim countMap() As Integer = GetCountMap(paints)
        FillGreedy(paints, countMap)
        Dim score As Integer = GetScore(paints)
        GetLocal16thSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal10thTo15thSolution(paints As BitArray, countMap() As Integer, maxRemoveNumber As Integer) As Solution
        paints = New BitArray(paints)
        countMap = TryCast(countMap.Clone(), Integer())
        IncreaseRemoveGreedy(paints, countMap, maxRemoveNumber)
        FillGreedy(paints, countMap)
        Dim score As Integer = GetScore(paints)
        GetLocal10thTo15thSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal3rdTo9thSolution(paints As BitArray, countMap() As Integer, maxRemoveNumber As Integer) As Solution
        paints = New BitArray(paints)
        countMap = TryCast(countMap.Clone(), Integer())
        RemoveGreedy(paints, countMap, maxRemoveNumber)
        FillGreedy(paints, countMap)
        Dim score As Integer = GetScore(paints)
        GetLocal3rdTo9thSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal1stSolution() As Solution
        Dim paints As BitArray = GetSimplePatternPaints(Kmax)
        Dim score As Integer = GetScore(paints)
        GetLocal1stSolution = New Solution(paints, score)
    End Function

    Private Function GetLocal23rdSolution(sol As Solution, maxCycle As Integer) As Solution
        Dim bestPaints As New BitArray(sol.Item1)
        Dim bestScore As Integer = sol.Item2

        Dim countMap() As Integer = GetCountMap(bestPaints)
        Dim paints As New BitArray(bestPaints)
        Dim score As Integer = bestScore

        For cycle As Integer = 1 To maxCycle
            For n As Integer = 0 To rand.Next(3, 7)
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If Not paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidRemove(countMap, py, px) Then
                        Remove(paints, countMap, py, px)
                        score -= n
                    End If
                Next i
                Shuffle(pIndexes(n))
            Next n
            For n As Integer = 9 To 0 Step -1
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidPaint(countMap, py, px) Then
                        Paint(paints, countMap, py, px)
                        score += n
                    End If
                Next i
            Next n
            If score > bestScore Then
                bestScore = score
                bestPaints = New BitArray(paints)
            End If
        Next cycle

        GetLocal23rdSolution = New Solution(bestPaints, bestScore)
    End Function

    Dim subGridPaints() As Boolean = Nothing
    Private Function GetRandomPetternPaints(Optional paints As BitArray = Nothing) As BitArray
        If subGridPaints Is Nothing Then
            Redim subGridPaints(subH * subW - 1)
            For i As Integer = 0 To Kmax - 1
                subGridPaints(i) = True
            Next i
        End If
        Shuffle(subGridPaints)
        If paints Is Nothing Then
            paints = New BitArray(H * W)
        End If
        For i As Integer = 0 To subH - 1
            For j As Integer = 0 To subW - 1
                Dim f As Boolean = subGridPaints(i * subW + j)
                For y As Integer = i To H - 1 Step subH
                    For x As Integer = j To W - 1 Step subW
                        paints(y * W + x) = f
                    Next x
                Next y
            Next j
        Next i
        GetRandomPetternPaints = paints
    End Function

    Private Function GetLocal24thSolution(maxCycle As Integer) As Solution
        Dim paints As BitArray = GetRandomPetternPaints()
        Dim score As Integer = GetScore(paints)
        Dim sol As New Solution(paints, score)
        GetLocal24thSolution = GetLocal23rdSolution(sol, maxCycle)
    End Function

    Dim nIndexes()() As Integer = Nothing
    Private Function GetLocal25thSolution(sol As Solution, maxCycle As Integer) As Solution
        Dim bestPaints As New BitArray(sol.Item1)
        Dim bestScore As Integer = sol.Item2

        If nIndexes Is Nothing Then
            ReDim nIndexes(9)
            For i As Integer = 0 To 9
                ReDim nIndexes(i)(i)
                For j As Integer = 0 To i
                    nIndexes(i)(j) = j
                Next j
            Next i
        End If

        Dim countMap() As Integer = GetCountMap(bestPaints)
        Dim paints As New BitArray(bestPaints)
        Dim score As Integer = bestScore

        For cycle As Integer = 1 To maxCycle
            Dim ni As Integer = rand.Next(6, 10)
            Shuffle(nIndexes(ni))
            For Each n As Integer In nIndexes(ni)
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If Not paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidRemove(countMap, py, px) Then
                        Remove(paints, countMap, py, px)
                        score -= n
                    End If
                Next i
                Shuffle(pIndexes(n))
            Next n
            Shuffle(nIndexes(9))
            For Each n As Integer In nIndexes(9)
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidPaint(countMap, py, px) Then
                        Paint(paints, countMap, py, px)
                        score += n
                    End If
                Next i
            Next n
            If score > bestScore Then
                bestScore = score
                bestPaints = New BitArray(paints)
            End If
        Next cycle

        GetLocal25thSolution = New Solution(bestPaints, bestScore)
    End Function

    Private Function GetLocal26thSolution(bestPaints As BitArray, bestScore As Integer, countMap() As Integer) As Solution
        Dim time0 As Integer = Environment.TickCount
        Dim time1 As Integer = time0
        Dim stopTime As Integer = startTime + 9500
        Dim interval As Integer = 0

        Dim paints As New BitArray(bestPaints)
        Dim score As Integer = bestScore
        countMap = TryCast(countMap.Clone(), Integer())
        Dim tmpPaints As BitArray = GetRandomPetternPaints()
        Dim tmpCountMap() As Integer = GetCountMap(tmpPaints)
        Dim tmpScore As Integer = GetScore(tmpPaints)
        Dim tmpCount As Integer = 0

        For cycle As Integer = 0 To 10000000
            time0 = time1
            time1 = Environment.TickCount
            interval = Math.Max(interval, time1 - time0)
            If time1 + interval > stopTime Then
                Console.Error.WriteLine("cycle: {0}, interval: {1} ms", cycle, interval)
                Exit For
            End If
            For n As Integer = 0 To rand.Next(5, 10)
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If Not paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidRemove(countMap, py, px) Then
                        Remove(paints, countMap, py, px)
                        score -= n
                    End If
                Next i
                Shuffle(ps)
            Next n
            For n As Integer = 9 To 0 Step -1
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If paints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidPaint(countMap, py, px) Then
                        Paint(paints, countMap, py, px)
                        score += n
                    End If
                Next i
                Shuffle(ps)
            Next n
            If score > bestScore Then
                bestScore = score
                bestPaints = New BitArray(paints)
            End If
            For n As Integer = 0 To rand.Next(5, 10)
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If Not tmpPaints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidRemove(tmpCountMap, py, px) Then
                        Remove(tmpPaints, tmpCountMap, py, px)
                        tmpScore -= n
                    End If
                Next i
                Shuffle(ps)
            Next n
            For n As Integer = 9 To 0 Step -1
                Dim ps() As Integer = points(n)
                For Each i As Integer In pIndexes(n)
                    Dim p As Integer = ps(i)
                    If tmpPaints(p) Then Continue For
                    Dim py As Integer = p \ W
                    Dim px As Integer = p - py * W
                    If CanValidPaint(tmpCountMap, py, px) Then
                        Paint(tmpPaints, tmpCountMap, py, px)
                        tmpScore += n
                    End If
                Next i
                Shuffle(ps)
            Next n
            If tmpScore > bestScore Then
                bestScore = tmpScore
                bestPaints = New BitArray(tmpPaints)
            End If
            tmpCount += 1
            If tmpCount > 30 Then
                tmpPaints = GetRandomPetternPaints(tmpPaints)
                tmpCountMap = GetCountMap(tmpPaints, tmpCountMap)
                tmpScore = GetScore(tmpPaints)
                tmpCount = 0
            End If
        Next cycle

        GetLocal26thSolution = New Solution(bestPaints, bestScore)
    End Function

End Class
Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console
Imports System.Diagnostics

Public Module Main

    Dim sw As New Stopwatch()

    Public Sub Main()
        Try
            sw.Start()
            Dim _pointsOnGrid As New PointsOnGrid()
            sw.Stop()

            ' do edit codes if you need

            CallFindSolution(_pointsOnGrid)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Function CallFindSolution(_pointsOnGrid As PointsOnGrid) As Integer
        Dim H As Integer = CInt(Console.ReadLine())
        Dim W As Integer = CInt(Console.ReadLine())
        Dim subH As Integer = CInt(Console.ReadLine())
        Dim subW As Integer = CInt(Console.ReadLine())
        Dim Kmin As Integer = CInt(Console.ReadLine())
        Dim Kmax As Integer = CInt(Console.ReadLine())
        Dim _gridSize As Integer = CInt(Console.ReadLine()) - 1
        Dim grid(_gridSize) As String
        For _idx As Integer = 0 To _gridSize
            grid(_idx) = Console.ReadLine()
        Next _idx

        sw.Start()
        Dim _result As String() = _pointsOnGrid.findSolution(H, W, subH, subW, Kmin, Kmax, grid)
        sw.Stop()
        Console.Error.WriteLine("time: {0} ms", sw.ElapsedMilliseconds)

        Console.WriteLine(_result.Length)
        For Each _it As String In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function
End Module